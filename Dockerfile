# Uso la imagen oficial de node como base
FROM node:latest as build

# Coloco el directorio de trabajo
WORKDIR /usr/local/app

# Parte 1: Compilar y armar aplicacion angular

# Copia el codigo fuente de la aplicacion angular

RUN git clone https://gitlab.com/lzucca/weather.git

# Instalo dependencias necesarias para la app
RUN cd /usr/local/app/weather && npm install

# Ingreso al directorio de la app y genero el build
RUN cd /usr/local/app/weather && npm run build

# Instalo apache dentro del contenedor

RUN apt update && apt install -y apache2 nano && apt clean && apt autoremove

# Se copia el conf para apache
COPY ./default.conf /etc/apache2/sites-available/

# Copio los archivos del build al directorio raiz de apache

RUN cp -r /usr/local/app/weather/dist/weather/* /var/www/html

# Parte 2: Compilar API NodeJS

# Copiamos el codigo fuente de la api

RUN git clone https://gitlab.com/lzucca/api.git

# Cambio los permisos de nodemon para poder levantar el servidor
RUN chmod +x /usr/local/app/api/node_modules/.bin/nodemon

# Ingresa al directorio e instala las dependencias necesarias para la api
RUN cd /usr/local/app/api && npm install

# Ingreso al directorio y dejo corriendo el servicio de la api
ENTRYPOINT cd /usr/local/app/api && npm run serve && service apache2 start

# Se expone el puerto 80
EXPOSE 80

