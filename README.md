Aplicacion de clima en angular2+ y nodejs

Paso 1: Clonar el proyecto.

Paso 2: Estando dentro de la carpeta clonada, armar imagen de la app. 

    docker build -t webapp

Paso 3: Levantar contenedor para reverse proxy con nginx.

    docker run -d --name reverseproxy -p 8888:80 -v /var/run/docker.sock:/tmp/docker.sock:ro jwilder/nginx-proxy

Paso 4: Levantar contenedor de nuestra app.

    docker run -d --name weather-app -e VIRTUAL_HOST=actualizacion.istea -p 80:80 webapp

Paso 5: Ingresar al navegador y navegar a la direccion 'actualizacion.istea:8888' (Debemos tener el archivo hosts de nuestra computadora apuntando a la direccion 'actualizacion.istea')
